﻿using Project.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class obe : Form
    {
        public obe()
        {
            InitializeComponent();
            subMenuPanelDesign();
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        private Form activeForm = null;
        private void openChildFormSign(Form childFormSign)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = childFormSign;
            childFormSign.TopLevel = false;
            childFormSign.FormBorderStyle = FormBorderStyle.None;
            childFormSign.Dock = DockStyle.Fill;
            panel2.Controls.Add(childFormSign);
            panel2.Tag = childFormSign;
            childFormSign.BringToFront();
            childFormSign.Show();
        }
        private void subMenuPanelDesign()
        {
            std.Visible = false;
            rub.Visible = false;
            asses.Visible = false;
            clos.Visible = false;
            panel4.Visible = false;
            panel5.Visible = false;
        }
        private void HideSubMenuPanels()
        {
            if (std.Visible == true)
            {
                std.Visible = false;
            }
            if (rub.Visible == true)
            {
                rub.Visible = false;
            }
            if (asses.Visible == true)
            {
                asses.Visible = false;
            }
            if (clos.Visible == true)
            {
                clos.Visible = false;
            }
            if (panel4.Visible == true)
            {
                panel4.Visible = false;
            } 
            if (panel5.Visible == true)
            {
                panel5.Visible = false;
            }
        }
        private void showSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                HideSubMenuPanels();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {

        }
        private void button10_Click(object sender, EventArgs e)
        {
            showSubMenu(rub);
        }
        private void button8_Click(object sender, EventArgs e)
        {
            showSubMenu(asses);
        }
        private void button22_Click(object sender, EventArgs e)
        {
            showSubMenu(clos);
        }
        private void button6_Click_1(object sender, EventArgs e)
        {
            showSubMenu(std);
        }
        private void button21_Click(object sender, EventArgs e)
        {
            openChildFormSign(new AddStudent());
            HideSubMenuPanels();
        }

        private void button20_Click(object sender, EventArgs e)//view stud
        {
            openChildFormSign(new viewstudents());
            HideSubMenuPanels();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            openChildFormSign(new deleteStud());
            HideSubMenuPanels();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            openChildFormSign(new updatestud());
            HideSubMenuPanels();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            showSubMenu(panel4);

        }


        private void button14_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new addAttendence());
        }

        private void button15_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new viewAttend());
        }

        private void button16_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new updateAttend());
        }

        private void button17_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new delAttend());
        }

        private void button11_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new AddClos());
        }

        private void button23_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new updateclo());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new Viewclo());
        }

        private void button9_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new addRub());
        }

        private void button13_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new updaterub());
        }

        private void button24_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new addRubricsLvl());
        }

        private void button25_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new updateRubricsLvl());
        }

        private void button7_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new AddAssesment());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new updateAsses());
        }

        private void button26_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new AddComponent());
        }

        private void button27_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new updateComponent());
        }

        private void button28_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new studentEvaluation());
        }


        private void button2_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            showSubMenu(panel5);
        }

        private void button33_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new assessmentComponent());
        }

        private void button32_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new assessmentwise());
        }

        private void button31_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new CLOwise());
        }

        private void button29_Click(object sender, EventArgs e)
        {
            HideSubMenuPanels();
            openChildFormSign(new AttendenceReport());
        }
    }
}
