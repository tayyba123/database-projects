﻿using project;
using project.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.GUI
{
    public partial class AttendenceReport : Form
    {
        public AttendenceReport()
        {
            InitializeComponent();
            attendance();
        }
        private void attendance()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select s.registrationNumber AS [Registration Number], " +
                "concat(s.firstname, s.lastname) AS Name, " +
                "(count(ca.id)/count(ca.id))*100 AS [Total Attendences %], " +
                "cast(cast((select count(*) from studentattendance sa join classattendance ca ON sa.attendanceid = ca.id where sa.studentid = s.id and sa.attendancestatus = 2) as decimal(6,2))/count(ca.id)*100 as decimal(6,2)) AS [Running Attendance %] " +
                "from student s " +
                "join studentattendance sa on sa.studentid = s.id " +
                "join classattendance ca on sa.attendanceid = ca.id " +
                "group by s.id, s.firstname, s.lastname, s.registrationnumber", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            List<StudentAttendance> queryResult = ExecuteQuery();
            PdfGenerator pdfGenerator = new PdfGenerator();
            pdfGenerator.GeneratePdfReport(queryResult,"Running Attendance Percentage Report of Class" );
        }
        private List<StudentAttendance> ExecuteQuery()
        {
            List<StudentAttendance> result = new List<StudentAttendance>();
            using (var con = Configuration.getInstance().getConnection())
            {
                SqlCommand cmd = new SqlCommand("select s.registrationNumber AS [Registration Number], " +
                                                "concat(s.firstname, ' ', s.lastname) AS Name, " +
                                                "(count(ca.id)*100/count(ca.id)) AS [Total Attendences %], " +
                                                "cast(cast((select count(*) from studentattendance sa join classattendance ca ON sa.attendanceid = ca.id where sa.studentid = s.id and sa.attendancestatus = 2) as decimal(6,2))/count(ca.id)*100 as decimal(6,2)) AS [Running Attendance %] " +
                                                "from student s " +
                                                "join studentattendance sa on sa.studentid = s.id " +
                                                "join classattendance ca on sa.attendanceid = ca.id " +
                                                "group by s.id, s.firstname, s.lastname, s.registrationnumber", con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    StudentAttendance studentAttendance = new StudentAttendance();
                    studentAttendance.RegistrationNumber = reader["Registration Number"].ToString();
                    studentAttendance.Name = reader["Name"].ToString();
                    studentAttendance.TotalAttendancesPercentage = Convert.ToDouble(reader["Total Attendences %"]);
                    studentAttendance.RunningAttendancePercentage = Convert.ToDouble(reader["Running Attendance %"]);

                    result.Add(studentAttendance);
                }
            }

            return result;
        }
        public class StudentAttendance
        {
            public string RegistrationNumber { get; set; }
            public string Name { get; set; }
            public double TotalAttendancesPercentage { get; set; }
            public double RunningAttendancePercentage { get; set; }
        }
    }
}
