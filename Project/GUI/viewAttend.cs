﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Project.GUI
{
    public partial class viewAttend : Form
    {
        public viewAttend()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DateTime date = selectedDatepicker.Value;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.AttendanceID,ss.RegistrationNumber as[ Registration No],concat(ss.firstname,' ',ss.lastname) as Name,l.name as Status from StudentAttendance s join ClassAttendance c on s.AttendanceID=c.id join Student ss on s.StudentID=ss.id join lookup l on s.AttendanceStatus=l.lookupid where c.AttendanceDate=@date  ", con);
            cmd.Parameters.AddWithValue("@date", date);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }
    }
}
