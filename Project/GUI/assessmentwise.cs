﻿using project;
using project.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Project.GUI.CLOwise;

namespace Project.GUI
{
    public partial class assessmentwise : Form
    {
        public assessmentwise()
        {
            InitializeComponent();
            addInComboBox();
            
        }
        private void addInComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select title from assessment", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr.GetString(0));

                }
            }
        }
        private int getassessIdFromName()
        {
            string assess = comboBox1.Text;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select id from assessment where title=@assess", con);
            cmd.Parameters.AddWithValue("@assess", assess);
            return (int)cmd.ExecuteScalar();

        }
        private void loadStudents()
        {
            int id = getassessIdFromName();
            this.dataGridView1.AllowUserToAddRows = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT s.id, s.registrationNumber AS [Registration No], CONCAT(s.firstname, ' ', s.lastname) AS Name, ase.title AS [Assessment Title], ase.totalmarks AS [Assessment Marks],asec.name AS [Component Name], " +
                 "CAST(ase.totalmarks * asec.totalmarks / (SELECT CAST(SUM(totalmarks) AS decimal(6,1)) FROM assessmentcomponent WHERE assessmentid = @id) AS decimal(6,1)) AS [Component Marks], " +
                 "CAST((CAST(ase.totalmarks * asec.totalmarks / (SELECT CAST(SUM(totalmarks) AS decimal(6,1)) FROM assessmentcomponent WHERE assessmentid = @id) AS decimal(6,1)) * sr.rubricmeasurementid) / MAX(rl.measurementlevel) AS decimal(6,1)) AS [Obtained Marks] " +
                 "FROM student s " +
                 "JOIN studentresult sr ON sr.studentid = s.id " +
                 "JOIN assessmentcomponent asec ON sr.assessmentcomponentid = asec.id " +
                 "JOIN assessment ase ON asec.assessmentid = ase.id " +
                 "JOIN rubric r ON asec.rubricid = r.id " +
                 "JOIN rubriclevel rl ON rl.rubricid = r.id " +
                 "WHERE ase.id = @id " +
                 "GROUP BY s.id, s.registrationNumber, s.firstname, s.lastname, ase.id, ase.title, ase.totalmarks, asec.name, asec.totalmarks, sr.rubricmeasurementid", con);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }
        private void loadStudent()
        {
            int id = getassessIdFromName();
            this.dataGridView1.AllowUserToAddRows = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(@"with TotalObtainedMarks AS (select sr.studentid,sr.assessmentcomponentid,
            cast((cast(ase.totalmarks * ac.totalmarks / (select cast(sum(totalmarks) AS decimal(6,1)) from assessmentcomponent where assessmentid = @id) AS decimal(6,1)) * sr.rubricmeasurementid) / max(rl.measurementlevel) AS decimal(6,1)) AS ObtainedMarks
            from studentresult sr
            join assessmentcomponent ac ON sr.assessmentcomponentid = ac.id
            join assessment ase ON ac.assessmentid = ase.id
            join rubric r ON ac.rubricid = r.id
            join rubriclevel rl ON r.id = rl.rubricid
            where ase.id = @id
            group by sr.studentid,sr.assessmentcomponentid,sr.rubricmeasurementid,ac.totalmarks,ase.totalmarks)
            SELECT s.id,s.registrationNumber AS [Registration No],CONCAT(s.firstname, ' ', s.lastname) AS Name,ase.title AS [Assessment Title],ase.totalmarks AS [Total Marks of Assessment],sum(c.ObtainedMarks) AS [Total Obtained Marks]
            from student s
            join TotalObtainedMarks c ON s.id = c.studentid
            join assessmentcomponent ac ON c.assessmentcomponentid = ac.id
            join assessment ase ON ac.assessmentid = ase.id
            where ase.id = @id
            GROUP BY s.id, s.registrationNumber, s.firstname, s.lastname, ase.title, ase.totalmarks;", con);

            cmd.Parameters.AddWithValue("@id", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            loadStudents();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            loadStudent();
        }
        private List<cloWiseResult> ExecuteQuery()
        {
          int  id = getassessIdFromName();
            List<cloWiseResult> result = new List<cloWiseResult>();
            using (var con = Configuration.getInstance().getConnection())
            {
                SqlCommand cmd = new SqlCommand("SELECT s.id, s.registrationNumber AS [Registration No], CONCAT(s.firstname, ' ', s.lastname) AS Name, ase.title AS [Assessment Title], ase.totalmarks AS [Assessment Marks],asec.name AS [Component Name], " +
                "CAST(ase.totalmarks * asec.totalmarks / (SELECT CAST(SUM(totalmarks) AS decimal(6,1)) FROM assessmentcomponent WHERE assessmentid = @id) AS decimal(6,1)) AS [Component Marks], " +
                "CAST((CAST(ase.totalmarks * asec.totalmarks / (SELECT CAST(SUM(totalmarks) AS decimal(6,1)) FROM assessmentcomponent WHERE assessmentid = @id) AS decimal(6,1)) * sr.rubricmeasurementid) / MAX(rl.measurementlevel) AS decimal(6,1)) AS [Obtained Marks] " +
                "FROM student s " +
                "JOIN studentresult sr ON sr.studentid = s.id " +
                "JOIN assessmentcomponent asec ON sr.assessmentcomponentid = asec.id " +
                "JOIN assessment ase ON asec.assessmentid = ase.id " +
                "JOIN rubric r ON asec.rubricid = r.id " +
                "JOIN rubriclevel rl ON rl.rubricid = r.id " +
                "WHERE ase.id = @id " +
                "GROUP BY s.id, s.registrationNumber, s.firstname, s.lastname, ase.id, ase.title, ase.totalmarks, asec.name, asec.totalmarks, sr.rubricmeasurementid", con);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    cloWiseResult cloWiseResult = new cloWiseResult();
                    cloWiseResult.RegistrationNumber = reader["Registration No"].ToString();
                    cloWiseResult.StudentName = reader["Name"].ToString();
                    cloWiseResult.AssessmentTitle = reader["Assessment Title"].ToString();
                    cloWiseResult.AssessmentMarks = Convert.ToDouble(reader["Assessment Marks"]);
                    cloWiseResult.ComponentName = reader["Component Name"].ToString();
                    cloWiseResult.ComponentMarks = Convert.ToDouble(reader["Component Marks"]);
                    cloWiseResult.ObtainedMarks = Convert.ToDouble(reader["Obtained Marks"]);
                    result.Add(cloWiseResult);
                }
            }
            return result;
        }
        public class cloWiseResult
        {
            public string RegistrationNumber { get; set; }
            public string StudentName { get; set; }
            public string AssessmentTitle { get; set; }
            public string ComponentName { get; set; }
            public double AssessmentMarks { get; set; }
            public double ComponentMarks { get; set; }
            public double ObtainedMarks { get; set; }
        }
        private List<assessmentwiseR> assessmentwisesExecute()
        {
           int id = getassessIdFromName();
            List<assessmentwiseR> result = new List<assessmentwiseR>();
            using (var con = Configuration.getInstance().getConnection())
            {
                SqlCommand cmd = new SqlCommand(@"with TotalObtainedMarks AS (select sr.studentid,sr.assessmentcomponentid,
                cast((cast(ase.totalmarks * ac.totalmarks / (select cast(sum(totalmarks) AS decimal(6,1)) from assessmentcomponent where assessmentid = @id) AS decimal(6,1)) * sr.rubricmeasurementid) / max(rl.measurementlevel) AS decimal(6,1)) AS ObtainedMarks
                from studentresult sr
                join assessmentcomponent ac ON sr.assessmentcomponentid = ac.id
                join assessment ase ON ac.assessmentid = ase.id
                join rubric r ON ac.rubricid = r.id
                join rubriclevel rl ON r.id = rl.rubricid
                where ase.id = @id
                group by sr.studentid,sr.assessmentcomponentid,sr.rubricmeasurementid,ac.totalmarks,ase.totalmarks)
                SELECT s.id,s.registrationNumber AS [Registration No],CONCAT(s.firstname, ' ', s.lastname) AS Name,ase.title AS [Assessment Title],ase.totalmarks AS [Assessment Marks],sum(c.ObtainedMarks) AS [Total Obtained Marks]
                from student s
                join TotalObtainedMarks c ON s.id = c.studentid
                join assessmentcomponent ac ON c.assessmentcomponentid = ac.id
                join assessment ase ON ac.assessmentid = ase.id
                where ase.id = @id
                GROUP BY s.id, s.registrationNumber, s.firstname, s.lastname, ase.title, ase.totalmarks;", con);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    assessmentwiseR cloWiseResult = new assessmentwiseR();
                    cloWiseResult.RegistrationNumber = reader["Registration No"].ToString();
                    cloWiseResult.StudentName = reader["Name"].ToString();
                    cloWiseResult.AssessmentTitle = reader["Assessment Title"].ToString();
                    cloWiseResult.AssessmentMarks = Convert.ToDouble(reader["Assessment Marks"]);
                    cloWiseResult.ObtainedMarks = Convert.ToDouble(reader["Total Obtained Marks"]);
                    result.Add(cloWiseResult);
                }
            }
            return result;
        }
        public class assessmentwiseR
        {
            public string RegistrationNumber { get; set; }
            public string StudentName { get; set; }
            public string AssessmentTitle { get; set; }
            public double AssessmentMarks { get; set; }
            public double ObtainedMarks { get; set; }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string pdf = comboBox2.Text;
            if(pdf == "Overall Result")
            {
                List<assessmentwiseR> queryResult = assessmentwisesExecute();
                PdfGenerator pdfGenerator = new PdfGenerator();
                pdfGenerator.GeneratePdfReport(queryResult, "Assessment Wise Report Of Class");
            }
            else if(pdf == "Result with each component")
            {
                List<cloWiseResult> queryResult = ExecuteQuery();
                PdfGenerator pdfGenerator = new PdfGenerator();
                pdfGenerator.GeneratePdfReport(queryResult, "Assessment Wise with separate component result Report Of Class");
            }
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Items.Add("Overall Result");
            comboBox2.Items.Add("Result with each component");
        }
    }
}
