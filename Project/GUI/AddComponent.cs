﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.GUI
{
    public partial class AddComponent : Form
    {
        public AddComponent()
        {
            InitializeComponent();
            addInComboBoxofAssessId();
            addInComboBoxofRubId();
            fillgrid();
        }
        private void addInComboBoxofAssessId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select title from Assessment", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox2.Items.Add(rdr.GetString(0));

                }
            }
        } 
        private void addInComboBoxofRubId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select details from Rubric", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr.GetString(0));

                }
            }
        }
        private int getrubricIdFromName()
        {
            string assess = comboBox1.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from rubric where details=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }
        }
        private int getassessIdFromName()
        {
            string assess = comboBox2.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from assessment where title=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }

        }
        private void button6_Click(object sender, EventArgs e)
        {
            string name = txtname.Text;
            int rubid= getrubricIdFromName();
            int assessid= getassessIdFromName();
            DateTime creatDate = selectedDatepicker.Value;
            DateTime updatedDate = dateTimePicker1.Value;
            string totalmarks = numericUpDown1.Value.ToString();

            if (!string.IsNullOrWhiteSpace(name))
            {
                if (rubid !=0)
                {
                    if (assessid!=0)
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent  values (@name,@rubid,@totalmarks,@creatDate,@updatedDate,@assessid)", con);
                        cmd.Parameters.AddWithValue("@name", name);
                        cmd.Parameters.AddWithValue("@rubid", rubid);
                        cmd.Parameters.AddWithValue("@totalmarks", totalmarks);
                        cmd.Parameters.AddWithValue("@creatDate", creatDate);
                        cmd.Parameters.AddWithValue("@updatedDate", updatedDate);
                        cmd.Parameters.AddWithValue("@assessid", assessid);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Added Successfully");
                        txtname.Text="";
                        selectedDatepicker.Value=DateTime.Now;
                        dateTimePicker1.Value=DateTime.Now;
                        numericUpDown1.Value = 1;
                        comboBox2.Text="";
                        comboBox1.Text="";
                        dataGridView1.DataSource = null;
                        fillgrid();
                    }
                    else
                    {
                        label14.Text = "Please select linked Assessment";
                        panel22.Visible = true;
                    }
                }
                else
                {
                    label7.Text = "Please select linked Rubric";
                    panel10.Visible = true;
                }
            }
            else
            {
                label8.Text = "Please enter Name";
                panel14.Visible = true;
            }
        }
        private void fillgrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select ac.Id as [Component Id],ac.name as [Component Name],ac.RubricId as [ Rubric Id],r.details as [Rubric Details],ac.assessmentid as [Assessment Id],a.title as [Assessment Name],c.id as [CLO ID],ac.totalmarks as [Component Marks],ac.datecreated as [Creation Date] ,ac.dateupdated as [Updation Date] from AssessmentComponent ac join rubric r on r.id = ac.rubricid join assessment a on  ac.assessmentid = a.id join clo c on r.cloid=c.id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }
    }
}
