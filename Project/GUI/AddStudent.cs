﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Collections.Specialized.BitVector32;
using System.Xml.Linq;

namespace Project.GUI
{
    public partial class AddStudent : Form
    {
        public AddStudent()
        {
            InitializeComponent();
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            errorpnl.Visible = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            panel5.Visible = false;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            panel11.Visible = false;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            panel8.Visible = false;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            panel14.Visible= false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string firstname=txtname.Text;
            string secname =txt2ndname.Text;
            string mail=txtmail.Text;   
            string contact=txtcontact.Text; 
            string regno=txtreg.Text;

            if (!string.IsNullOrWhiteSpace(firstname))
            {
                if (!string.IsNullOrWhiteSpace(secname))
                {
                    if (!string.IsNullOrWhiteSpace(mail))
                    {
                        if (mail.EndsWith("@gmail.com"))
                        {
                            if (!string.IsNullOrWhiteSpace(contact))
                            {
                                if ((isValidContact(contact) && contact.Length == 11))
                                {
                                    if (!string.IsNullOrWhiteSpace(regno))
                                    {
                                        var con = Configuration.getInstance().getConnection();
                                        SqlCommand cmd = new SqlCommand("Insert into Student values (@firstname, @secname,@contact,@mail,@regno,@stts)", con);
                                        cmd.Parameters.AddWithValue("@firstname", txtname.Text);
                                        cmd.Parameters.AddWithValue("@secname", txt2ndname.Text);
                                        cmd.Parameters.AddWithValue("@mail", txtmail.Text);
                                        cmd.Parameters.AddWithValue("@contact", txtcontact.Text);
                                        cmd.Parameters.AddWithValue("@regno", txtreg.Text);
                                        cmd.Parameters.AddWithValue("@stts", 5);
                                        cmd.ExecuteNonQuery();
                                        MessageBox.Show("Successfully saved");
                                        txtname.Text = "";
                                        txt2ndname.Text = "";
                                        txtmail.Text = "";
                                        txtcontact.Text = "";
                                        txtreg.Text = "";
                                    }
                                    else
                                    {
                                        label10.Text = "Please enter Registration number";
                                        panel14.Visible = true;
                                    }
                                }
                                else
                                {
                                    label6.Text = "Incorrect Contact number";
                                    panel8.Visible = true;
                                }
                            }
                            else
                            {
                                label6.Text = "Please enter Contact number";
                                panel8.Visible = true;
                            }
                        }
                        else
                        {
                            label8.Text = "Please enter email correctly";
                            panel11.Visible = true;
                        }
                    }
                    else
                    {
                        label8.Text = "Please enter Email";
                        panel11.Visible = true;
                    }
                }
                else
                {
                    label4.Text = "Please enter second name";
                    panel5.Visible = true;
                }
            }
            else
            {
                label1.Text = "Please enter first name";
                errorpnl.Visible=true;
            }

        }
        private bool isValidContact(string contact)
        {
            foreach (char i in contact)
            {
                if ((int)i < 48 || (int)i > 57)
                {
                    MessageBox.Show(i.ToString());
                    return false;
                }
            }
            return true;
        }
    }
}
