﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Project.GUI
{
    public partial class updateRubricsLvl : Form
    {
        public updateRubricsLvl()
        {
            InitializeComponent();
            addInComboBoxofRubId();
            addInComboBoxofRubLvlId();
            fillgrid(); 
        }
        private void addInComboBoxofRubId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select details from Rubric", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    cmb_ruberics.Items.Add(rdr.GetString(0));
                }
            }
        } 
        private void addInComboBoxofRubLvlId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select id from RubricLevel", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr.GetInt32(0).ToString());
                }
            }
        }
        private int getrubIdFromName()
        {
            string assess = cmb_ruberics.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from rubric where details=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }
        }
       
        private void fillgrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select l.Id as [Level Id],l.RubricId as [Rubric Id],r.Details as [Rubric Details],l.details as [Level Details],l.MeasurementLevel as Marks,c.id as [CLO Id] , c.name as [CLO Name] from RubricLevel l join rubric r on r.id=l.rubricid join clo c on r.cloid=c.id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            string id = comboBox1.Text;
            int rid = getrubIdFromName();
            if (!string.IsNullOrWhiteSpace(id))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE RubricLevel SET RubricId=CASE WHEN  @RubId ='' or @RubId is null THEN RubricId ELSE @RubId END,Details= CASE WHEN @details='' or @details is NULL THEN  Details ELSE  @details END, MeasurementLevel=case when @measuelvl='' or @measuelvl is null then MeasurementLevel else @measuelvl end  where Id=@id", con);
                cmd.Parameters.AddWithValue("@details", txtname.Text);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@RubId", rid);
                cmd.Parameters.AddWithValue("@measuelvl", numericUpDown1.Value.ToString());
                cmd.ExecuteNonQuery();
                dataGridView1.DataSource = null;
                fillgrid();
                MessageBox.Show("Successfully Updated");
                txtname.Text = "";
                cmb_ruberics.Text = "";
                numericUpDown1.Value = 1;
                comboBox1.Text = "";
                comboBox1.Items.Clear();
                cmb_ruberics.Items.Clear();
                addInComboBoxofRubLvlId();
                addInComboBoxofRubId();
            }
            else
            {
                label4.Text = "Please select Rubric level for Updation";
                panel14.Visible = true;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel14.Visible = false;
        }

    }
}
