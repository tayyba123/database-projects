﻿using project;
using project.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.GUI
{
    public partial class viewstudents : Form
    {
        public viewstudents()
        {
            InitializeComponent();
            addInComboBox();
        }
        private void addInComboBox()
        {
            comboBox3.Items.Add("Active Students");
            comboBox3.Items.Add("InActive Students");
            comboBox3.Items.Add("Download pdf");
        }
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string val=comboBox3.Text.ToString();
            if(val == "Active Students")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select s.id,concat(s.firstname,' ',s.lastname) as Name,s.RegistrationNumber as [Registration No],s.contact,s.email,l.name as status from student s join lookup l on s.status=l.LookupId where l.lookupid= 5", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
            }
            else if(val == "InActive Students")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select s.id,concat(s.firstname,' ',s.lastname),s.RegistrationNumber as [Registration No],s.contact,s.email,l.name as status from student s join lookup l on s.status=l.LookupId where l.lookupid= 6", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
            }
            else if(val == "Download pdf")
            {
                List<cloWiseResult> queryResult = ExecuteQuery();
                PdfGenerator pdfGenerator = new PdfGenerator();
                pdfGenerator.GeneratePdfReport(queryResult, "All Active Students Report");
            }
        }
        private List<cloWiseResult> ExecuteQuery()
        {
            List<cloWiseResult> result = new List<cloWiseResult>();
            using (var con = Configuration.getInstance().getConnection())
            {
                SqlCommand cmd = new SqlCommand("Select s.id as ID,concat(s.firstname,' ',s.lastname) as Name,s.RegistrationNumber as [Registration No],l.name as Status from student s join lookup l on s.status=l.LookupId where l.lookupid= 5", con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    cloWiseResult cloWiseResult = new cloWiseResult();
                    cloWiseResult.ID = reader["ID"].ToString();
                    cloWiseResult.StudentName = reader["Name"].ToString();
                    cloWiseResult.RegistrationNo = reader["Registration No"].ToString();
                    cloWiseResult.Status = reader["Status"].ToString();
                    result.Add(cloWiseResult);
                }
            }
            return result;
        }
        public class cloWiseResult
        {
            public string ID { get; set; }
            public string StudentName { get; set; }
            public string RegistrationNo { get; set; }
            public string Status { get; set; }
        }

    }
}
