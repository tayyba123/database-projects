﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.GUI
{
    public partial class AddClos : Form
    {
        public AddClos()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string name=txtname.Text;
            if (!string.IsNullOrWhiteSpace(name))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Clo values (@name, @creationDate,@updationDate)", con);
                cmd.Parameters.AddWithValue("@name", txtname.Text);
                cmd.Parameters.AddWithValue("@creationDate", selectedDatepicker.Value);
                cmd.Parameters.AddWithValue("@updationDate", selectedDatepicker.Value);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                txtname.Text="";
                selectedDatepicker.Value=DateTime.Now;
            }
            else
            {
                label3.Text = "Please enter the name";
                errorpnl.Visible = true;
            }
        }
    }
}
