﻿using project;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Project.GUI
{
    public partial class addRub : Form
    {
        int RubId;
        public addRub()
        {
            InitializeComponent();
            addInComboBox();
            generateid();
            loadStudents();
        }
        private void addInComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select name from Clo", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    cmb_ruberics.Items.Add(rdr.GetString(0));
                }
            }
        }
        private int getassessIdFromName()
        {
            string assess = cmb_ruberics.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from clo where name=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }

        }
        private void generateid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select count(*) from Rubric", con);
            this.RubId = (int)cmd.ExecuteScalar() + 1;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            string details=txtname.Text;
            int cloid = getassessIdFromName();
            if (cloid!=0)
            {
                if (!string.IsNullOrWhiteSpace(details))
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Insert into Rubric values (@id,@details, @cloid)", con);
                    cmd.Parameters.AddWithValue("@cloid", cloid);
                    cmd.Parameters.AddWithValue("@id", RubId);
                    cmd.Parameters.AddWithValue("@details", txtname.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                    txtname.Text="";
                    cmb_ruberics.Text = "";
                    dataGridView1.DataSource = null;
                    loadStudents();
                }
                else
                {
                    label3.Text = "Please enter Details";
                    errorpnl.Visible = true;
                }
            }
            else
            {
                label7.Text = "Please select linked Clo";
                panel10.Visible = true;
            }
        }
        private void loadStudents()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select r.Id as [Rubric Id],r.details as [Rubric Details],r.cloid as [CLO Id],c.name as [CLO Name]from Rubric r join Clo c on c.id=r.cloid", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows= false;
            dataGridView1.ReadOnly = true;
        }
    }
}
