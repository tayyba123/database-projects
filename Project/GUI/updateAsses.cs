﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.GUI
{
    public partial class updateAsses : Form
    {
        public updateAsses()
        {
            InitializeComponent();
            addInComboBoxofRubId();
            fillgrid();
        }
        private void addInComboBoxofRubId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select title from Assessment", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                   // comboBox1.Items.Add(rdr.GetInt32(0).ToString());  for int as i was doing it with id previously
                    comboBox1.Items.Add(rdr.GetString(0));
                }
            }
        }
        private int getassessIdFromName()
        {
            string assess = comboBox1.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from assessment where title=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }
        }
        private void fillgrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id,Title,datecreated,totalmarks,totalweightage from Assessment ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            int id = getassessIdFromName();
            if(id !=0 )                         //(!string.IsNullOrWhiteSpace(id)) for string
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE Assessment SET Title=case when @title='' or @title is null then Title else @title end, DateCreated=CASE WHEN  @date ='' or @date is null THEN DateCreated ELSE @date END,TotalMarks= CASE WHEN @marks='' or @marks is NULL THEN  TotalMarks ELSE  @marks END ,TotalWeightage= CASE WHEN @weightage='' or @weightage is NULL THEN  TotalWeightage ELSE  @weightage END where Id=@id", con);
                cmd.Parameters.AddWithValue("@title", txtname.Text);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@date", selectedDatepicker.Value);
                cmd.Parameters.AddWithValue("@marks", numericUpDown1.Value.ToString());
                cmd.Parameters.AddWithValue("@weightage", numericUpDown2.Value.ToString());
                cmd.ExecuteNonQuery();
                dataGridView1.DataSource = null;
                fillgrid();
                MessageBox.Show("Successfully Updated");
                txtname.Text = "";
                selectedDatepicker.Value=DateTime.Now;
                numericUpDown1.Value = 1;
                numericUpDown2.Value = 1;
                comboBox1.Text = "";
                comboBox1.Items.Clear();
                addInComboBoxofRubId();
            }
            else
            {
                label10.Text = "Please select assessment for Updation";
                panel16.Visible = true;
            }
        }
    }
}
