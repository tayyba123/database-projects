﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.GUI
{
    public partial class delAttend : Form
    {
        public delAttend()
        {
            InitializeComponent();
            loadStudents();
        }
        private void loadStudents()
        {
            this.dataGridView1.AllowUserToAddRows = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select id,AttendanceDate from ClassAttendance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            this.dataGridView1.AllowUserToAddRows = false;

        }
        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                int classAttendId = int.Parse(row.Cells["id"].Value.ToString());
                var con = Configuration.getInstance().getConnection();
                string query = "Delete from StudentAttendance  WHERE AttendanceId = @classAttendId";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@classAttendId", classAttendId);
                cmd.ExecuteNonQuery();

                con = Configuration.getInstance().getConnection();
                string query2 = "Delete from ClassAttendance  WHERE id = @classAttendId";
                SqlCommand cmd2 = new SqlCommand(query2, con);
                cmd2.Parameters.AddWithValue("@classAttendId", classAttendId);
                cmd2.ExecuteNonQuery();
                loadStudents();
            }
        }
    }
}
