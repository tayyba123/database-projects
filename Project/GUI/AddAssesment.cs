﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Project.GUI
{
    public partial class AddAssesment : Form
    {
        public AddAssesment()
        {
            InitializeComponent();
            fillgrid();
        }
        private void button6_Click(object sender, EventArgs e)
        {
            string name=txtname.Text;
            DateTime date=selectedDatepicker.Value;
            string marks=numericUpDown1.Value.ToString();
            string weightage=numericUpDown2.Value.ToString();

            if (!string.IsNullOrWhiteSpace(name))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Assessment  values (@name,@date,@marks,@weightage)", con);
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@date", date);
                cmd.Parameters.AddWithValue("@marks", marks);
                cmd.Parameters.AddWithValue("@weightage", weightage);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Added Successfully");
                txtname.Text = "";
                selectedDatepicker.Value=DateTime.Now;
                numericUpDown1.Value = 1;
                numericUpDown2.Value = 1;
                dataGridView1.DataSource = null;
                fillgrid();
            }
            else
            {
                label8.Text = "Please enter the Title";
                panel14.Visible = true;
            }
        }
        private void fillgrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id,Title,datecreated,totalmarks,totalweightage from Assessment ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
        }
    }
}
