﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.GUI
{
    public partial class deleteStud : Form
    {
        public deleteStud()
        {
            InitializeComponent();
            loadStudents();
        }
        private void loadStudents()
        {
            this.dataGridView1.AllowUserToAddRows = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.id,concat(s.firstname,' ',s.lastname) as Name," +
                "s.RegistrationNumber as [Registration No],l.name as Status from student s join lookup l" +
                " on s.status=l.LookupId where l.lookupid= 5" , con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            this.dataGridView1.AllowUserToAddRows = false;
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                int studid = int.Parse(row.Cells["id"].Value.ToString());
                var con = Configuration.getInstance().getConnection();
                string query = "update Student set status=6 WHERE id = @studid";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@studid", studid);
                cmd.ExecuteNonQuery();
                loadStudents();

            }
        }
    }
}
