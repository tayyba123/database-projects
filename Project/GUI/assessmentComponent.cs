﻿using project;
using project.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.GUI
{
    public partial class assessmentComponent : Form
    {
        int assesscompid;
        int assessid;
        public assessmentComponent()
        {
            InitializeComponent();
            addInComboBox();
        }
        private void addInComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select title from assessment", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr.GetString(0));

                }
            }
        }
        private int getassessIdFromName()
        {
            string assess = comboBox1.Text;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select id from assessment where title=@assess", con);
            cmd.Parameters.AddWithValue("@assess", assess);
            return (int)cmd.ExecuteScalar();

        }
        private int getassessCompIdFromName()
        {
            string assess = comboBox2.Text;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select id from assessmentcomponent where name=@assess", con);
            cmd.Parameters.AddWithValue("@assess", assess);
            return (int)cmd.ExecuteScalar();

        }
        private void addInComboBoxAssessComp()
        {
            assessid = getassessIdFromName();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select name from assessmentComponent where assessmentid=@id", con);
            cmd.Parameters.AddWithValue("@id", assessid);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox2.Items.Add(rdr.GetString(0));

                }
            }
        }
        private void loadStudents()
        {
            assesscompid = getassessCompIdFromName();
            this.dataGridView1.AllowUserToAddRows = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT s.id, s.registrationNumber AS [Registration No], CONCAT(s.firstname, ' ', s.lastname) AS Name, ase.title AS [Assessment Title], ase.totalmarks AS [Assessment Marks], asec.name AS [Component Name], asec.totalmarks AS [Component Marks], ((asec.totalmarks * sr.rubricmeasurementid)/MAX(rl.measurementlevel)) AS [Obtained Marks] " +
                                "FROM student s " +
                                "JOIN studentresult sr ON sr.studentid = s.id " +
                                "JOIN assessmentcomponent asec ON sr.assessmentcomponentid = asec.id " +
                                "JOIN assessment ase ON asec.assessmentid = ase.id " +
                                "JOIN rubric r ON asec.rubricid = r.id " +
                                "JOIN rubriclevel rl ON rl.rubricid = r.id " +
                                "WHERE sr.assessmentcomponentid = @id " + 
                                "GROUP BY s.id, s.registrationNumber, s.firstname, s.lastname, ase.title, ase.totalmarks, asec.name, asec.totalmarks,sr.rubricmeasurementid", con);
            cmd.Parameters.AddWithValue("@id", assesscompid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows=false;
            dataGridView1.ReadOnly = true;
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            addInComboBoxAssessComp();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadStudents();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            List<cloWiseResult> queryResult = ExecuteQuery();
            PdfGenerator pdfGenerator = new PdfGenerator();
            pdfGenerator.GeneratePdfReport(queryResult, "Assessment Component Wise Report Of Class");
        }
        private List<cloWiseResult> ExecuteQuery()
        {
            List<cloWiseResult> result = new List<cloWiseResult>();
            using (var con = Configuration.getInstance().getConnection())
            {
                SqlCommand cmd = new SqlCommand("SELECT s.id, s.registrationNumber AS [Registration No], CONCAT(s.firstname, ' ', s.lastname) AS Name, ase.title AS [Assessment Title], ase.totalmarks AS [Assessment Marks], asec.name AS [Component Name], asec.totalmarks AS [Component Marks], ((asec.totalmarks * sr.rubricmeasurementid)/MAX(rl.measurementlevel)) AS [Obtained Marks] " +
                                "FROM student s " +
                                "JOIN studentresult sr ON sr.studentid = s.id " +
                                "JOIN assessmentcomponent asec ON sr.assessmentcomponentid = asec.id " +
                                "JOIN assessment ase ON asec.assessmentid = ase.id " +
                                "JOIN rubric r ON asec.rubricid = r.id " +
                                "JOIN rubriclevel rl ON rl.rubricid = r.id " +
                                "WHERE sr.assessmentcomponentid = @id " +
                                "GROUP BY s.id, s.registrationNumber, s.firstname, s.lastname, ase.title, ase.totalmarks, asec.name, asec.totalmarks,sr.rubricmeasurementid", con);
                cmd.Parameters.AddWithValue("@id", assesscompid);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    cloWiseResult cloWiseResult = new cloWiseResult();
                    cloWiseResult.RegistrationNumber = reader["Registration No"].ToString();
                    cloWiseResult.StudentName = reader["Name"].ToString();
                    cloWiseResult.AssessmentTitle = reader["Assessment Title"].ToString();
                    cloWiseResult.AssessmentMarks = Convert.ToDouble(reader["Assessment Marks"]);
                    cloWiseResult.ComponentName = reader["Component Name"].ToString();
                    cloWiseResult.ComponentMarks = Convert.ToDouble(reader["Component Marks"]);
                    cloWiseResult.ObtainedMarks = Convert.ToDouble(reader["Obtained Marks"]);
                    result.Add(cloWiseResult);
                }
            }
            return result;
        }
        public class cloWiseResult
        {
            public string RegistrationNumber { get; set; }
            public string StudentName { get; set; }
            public string AssessmentTitle { get; set; }
            public string ComponentName { get; set; }
            public double AssessmentMarks { get; set; }
            public double ComponentMarks { get; set; }
            public double ObtainedMarks { get; set; }
        }
    }
}
