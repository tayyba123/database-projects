﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.GUI
{
    public partial class evaluate : Form
    {
        int sid;
        int rubid;
        int marks;
        int assesscompid;
        private string[] detailsArr = new string[10];
        public evaluate(int sid, int assesscompid)
        {
            InitializeComponent();
            this.sid = sid;
            this.assesscompid = assesscompid;
            trackBar1.Minimum = 0;
            getmaxlvlmarks();
            fillComLable();
            trackBar1.Maximum = marks;
            trackBar1.TickFrequency = 1;
            trackBar1.TickStyle = TickStyle.None;
            trackBar1.ValueChanged += trackBar1_ValueChanged;
        }
        private void fillComLable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select name from assessmentcomponent where id=@id", con);
            cmd.Parameters.AddWithValue("@id", assesscompid);
            string name= (string)cmd.ExecuteScalar();
            lblassc.Text = name;
        }
        private int getid()
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select rubricid from assessmentcomponent where id=@id", con);
                cmd.Parameters.AddWithValue("@id", assesscompid);
                return (int)cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void getmaxlvlmarks()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select max(measurementlevel) from rubriclevel where rubricid=(Select rubricid from assessmentcomponent where id=@id)", con);
            cmd.Parameters.AddWithValue("@id", assesscompid);
            marks = (int)cmd.ExecuteScalar();
            lblmarks.Text = marks.ToString();
        }
       

        private void button6_Click(object sender, EventArgs e)
        {
            if (trackBar1.Value>0)
            {
                int marks = trackBar1.Value;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into StudentResult values (@sid, @assesscompid,@measurmentid,@date)", con);
                cmd.Parameters.AddWithValue("@sid", sid);
                cmd.Parameters.AddWithValue("@assesscompid", assesscompid);
                cmd.Parameters.AddWithValue("@measurmentid", marks);
                cmd.Parameters.AddWithValue("@date", DateTime.Now);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }
        }
        private void inarr()
        {
            rubid = getid();
            if (rubid != 0)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand("Select  STRING_AGG(convert(nvarchar(max),Details), ',') from rubriclevel where rubricid=@id", con);
                cmd2.Parameters.AddWithValue("@id", rubid);
                string details = (string)cmd2.ExecuteScalar();
                detailsArr = details.Split(',');
            }
        }
        private int getmarks(string remarks)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Select  measurementlevel from rubriclevel where rubricid=@id and details=@detail", con);
            cmd3.Parameters.AddWithValue("@id", rubid);
            cmd3.Parameters.AddWithValue("@detail", remarks);
            object result = cmd3.ExecuteScalar();
            int marks = result != null ? (int)result : 0;
            return marks;
        }
        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            inarr();
            try {
                switch (trackBar1.Value)
                {
                    case 1:
                        lblremarks.Text = detailsArr[0];
                        lblobtain.Text = getmarks(detailsArr[0]).ToString();
                        break;
                    case 2:
                        lblremarks.Text = detailsArr[1];
                        lblobtain.Text = getmarks(detailsArr[1]).ToString();

                        break;
                    case 3:
                        lblremarks.Text = detailsArr[2];
                        lblobtain.Text = getmarks(detailsArr[2]).ToString();

                        break;
                    case 4:
                        lblremarks.Text = detailsArr[3];
                        lblobtain.Text = getmarks(detailsArr[3]).ToString();

                        break;
                    case 5:
                        lblremarks.Text = detailsArr[4];
                        lblobtain.Text = getmarks(detailsArr[4]).ToString();

                        break;
                    default:
                        lblremarks.Text = "";
                        lblobtain.Text = "";
                        break;
                }

                lblremarks.Visible = trackBar1.Value != 0;
                lblobtain.Visible = trackBar1.Value != 0;
            }catch(Exception ex) { MessageBox.Show("Your rubric have less levels as required in normal rubric this will cause some fluctuation in data"); }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

        }
    }
    
}
