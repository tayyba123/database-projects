﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.GUI
{
    public partial class updateComponent : Form
    {
        public updateComponent()
        {
            InitializeComponent();
            addInComboBoxofComponentId();
            addInComboBoxofRubId();
            addInComboBoxofAssessId();
            fillgrid();
        }
        private void addInComboBoxofAssessId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select title from Assessment", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox2.Items.Add(rdr.GetString(0));

                }
            }
        }
        private void addInComboBoxofRubId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select details from Rubric", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr.GetString(0));

                }
            }
        }
        private void addInComboBoxofComponentId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select name from AssessmentComponent", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox3.Items.Add(rdr.GetString(0));

                }
            }
        }
        private void fillgrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select ac.Id as [Component Id],ac.name as [Component Name],ac.RubricId as [ Rubric Id],r.details as [Rubric Details],ac.assessmentid as [Assessment Id],a.title as [Assessment Name],c.id as [CLO ID],ac.totalmarks as [Component Marks],ac.datecreated as [Creation Date] ,ac.dateupdated as [Updation Date] from AssessmentComponent ac join rubric r on r.id = ac.rubricid join assessment a on  ac.assessmentid = a.id join clo c on r.cloid=c.id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }
        private int getassessIdFromName()
        {
            string assess = comboBox2.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from assessment where title=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }

        }
        private int getrubricIdFromName()
        {
            string assess = comboBox1.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from rubric where details=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }
        }
        private int getcompIdFromName()
        {
            string assess = comboBox3.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from assessmentcomponent where name=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            int rubid = getrubricIdFromName();
            int assessid = getassessIdFromName();
            int id = getcompIdFromName();
            if (id!=0)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE AssessmentComponent SET Name=CASE WHEN  @name ='' or @name is null THEN Name ELSE @name END,RubricId= CASE WHEN @RubId=0 THEN  RubricId ELSE  @RubId END, totalmarks=case when @marks='' or @marks is null then totalmarks else @marks end,datecreated=CASE WHEN  @datecreated ='' or @datecreated is null THEN datecreated ELSE @datecreated END,dateupdated=CASE WHEN  @dateupdated ='' or @dateupdated is null THEN dateupdated ELSE @dateupdated END,assessmentid=CASE WHEN  @assessmentid =0  THEN assessmentid ELSE @assessmentid END  where Id=@id", con);
                cmd.Parameters.AddWithValue("@name", txtname.Text);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@RubId", rubid);
                cmd.Parameters.AddWithValue("@assessmentid", assessid);
                cmd.Parameters.AddWithValue("@marks",numericUpDown1.Value.ToString());
                cmd.Parameters.AddWithValue("@datecreated",selectedDatepicker.Value) ;
                cmd.Parameters.AddWithValue("@dateupdated",dateTimePicker1.Value);
                cmd.ExecuteNonQuery();
                dataGridView1.DataSource = null;
                fillgrid();
                MessageBox.Show("Successfully Updated");
                txtname.Text = "";
                comboBox3.Text = "";
                comboBox2.Text = "";
                comboBox1.Text = "";
                selectedDatepicker.Value = DateTime.Now;
                dateTimePicker1.Value = DateTime.Now;
                numericUpDown1.Value = 1;
                comboBox1.Items.Clear();
                comboBox2.Items.Clear();
                comboBox3.Items.Clear();
                addInComboBoxofComponentId();
                addInComboBoxofRubId();
                addInComboBoxofAssessId();
            }
            else
            {
                label5.Text = "Please enter Id from Updation";
                panel16.Visible = true;
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel16.Visible = false;
        }
    }
}
