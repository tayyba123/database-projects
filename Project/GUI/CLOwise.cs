﻿using project;
using project.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Project.GUI.AttendenceReport;

namespace Project.GUI
{
    public partial class CLOwise : Form
    {
        int id;
        public CLOwise()
        {
            InitializeComponent();
            addInComboBox();
        }
        private void addInComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select name from clo", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr.GetString(0));

                }
            }
        }
        private int getcloIdFromName()
        {
            string assess = comboBox1.Text;
            if (!string.IsNullOrEmpty(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from clo where name=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }
        }
        private void loadStudents()
        {
             id = getcloIdFromName();
            this.dataGridView1.AllowUserToAddRows = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT s.id, s.registrationNumber AS [Registration No], CONCAT(s.firstname, ' ', s.lastname) AS Name,c.name as CLO, ase.title AS [Assessment Title], ase.totalmarks AS [Assessment Marks], asec.name AS [Component Name], " +
                "CAST(ase.totalmarks * asec.totalmarks / (SELECT CAST(SUM(totalmarks) AS decimal(6,1)) FROM assessmentcomponent WHERE assessmentid = ase.id) AS decimal(6,1)) AS [Component Marks], " +
                "CAST((CAST(ase.totalmarks * asec.totalmarks / (SELECT CAST(SUM(totalmarks) AS decimal(6,1)) FROM assessmentcomponent WHERE assessmentid = ase.id) AS decimal(6,1)) * sr.rubricmeasurementid) / MAX(rl.measurementlevel) AS decimal(6,1)) AS [Obtained Marks] " +
                "FROM student s " +
                "JOIN studentresult sr ON sr.studentid = s.id " +
                "JOIN assessmentcomponent asec ON sr.assessmentcomponentid = asec.id " +
                "JOIN assessment ase ON asec.assessmentid = ase.id " +
                "JOIN rubric r ON asec.rubricid = r.id " +
                "JOIN clo c ON r.cloid = c.id " +
                "JOIN rubriclevel rl ON rl.rubricid = r.id " +
                "WHERE c.id = @id " +
                "GROUP BY s.id, s.registrationNumber, s.firstname, s.lastname,c.name, ase.id, ase.title, ase.totalmarks, asec.name, asec.totalmarks, sr.rubricmeasurementid", con);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }
        private void loadStudent()
        {
             id = getcloIdFromName();
            this.dataGridView1.AllowUserToAddRows = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT s.id, s.registrationNumber AS [Registration No], CONCAT(s.firstname, ' ', s.lastname) AS Name,c.name as CLO,sum(ase.totalmarks) as [CLO Total Marks] " +
                "CAST(sum(ase.totalmarks) * ase.totalmarks / (SELECT CAST(SUM(a.totalmarks) AS decimal(6,1)) FROM assessment a join assessmentcomponent aa on a.id=aa.assessmentid join rubric r on aa.rubricid=r.id WHERE r.cloid = @id) AS decimal(6,1)) AS [Assessment Marks], " +
                "CAST((CAST(ase.totalmarks * asec.totalmarks / (SELECT CAST(SUM(totalmarks) AS decimal(6,1)) FROM assessmentcomponent WHERE assessmentid = ase.id) AS decimal(6,1)) * sr.rubricmeasurementid) / MAX(rl.measurementlevel) AS decimal(6,1)) AS [Obtained Marks] " +
                "FROM student s " +
                "JOIN studentresult sr ON sr.studentid = s.id " +
                "JOIN assessmentcomponent asec ON sr.assessmentcomponentid = asec.id " +
                "JOIN assessment ase ON asec.assessmentid = ase.id " +
                "JOIN rubric r ON asec.rubricid = r.id " +
                "JOIN clo c ON r.cloid = c.id " +
                "JOIN rubriclevel rl ON rl.rubricid = r.id " +
                "WHERE c.id = @id " +
                "GROUP BY s.id, s.registrationNumber, s.firstname, s.lastname,c.name, ase.id, ase.title, ase.totalmarks, asec.name, asec.totalmarks, sr.rubricmeasurementid", con);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadStudents();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            List<cloWiseResult> queryResult = ExecuteQuery();
            PdfGenerator pdfGenerator = new PdfGenerator();
            pdfGenerator.GeneratePdfReport(queryResult, "CLO Wise Report Of Class");
        }
        private List<cloWiseResult> ExecuteQuery()
        {
            List<cloWiseResult> result = new List<cloWiseResult>();
            using (var con = Configuration.getInstance().getConnection())
            {
                SqlCommand cmd = new SqlCommand("SELECT s.id, s.registrationNumber AS [Registration No], CONCAT(s.firstname, ' ', s.lastname) AS Name,c.name as CLO, ase.title AS [Assessment Title], ase.totalmarks AS [Assessment Marks], asec.name AS [Component Name], " +
                                "CAST(ase.totalmarks * asec.totalmarks / (SELECT CAST(SUM(totalmarks) AS decimal(6,1)) FROM assessmentcomponent WHERE assessmentid =ase.id) AS decimal(6,1)) AS [Component Marks], " +
                                "CAST((CAST(ase.totalmarks * asec.totalmarks / (SELECT CAST(SUM(totalmarks) AS decimal(6,1)) FROM assessmentcomponent WHERE assessmentid = ase.id) AS decimal(6,1)) * sr.rubricmeasurementid) / MAX(rl.measurementlevel) AS decimal(6,1)) AS [Obtained Marks] " +
                                "FROM student s " +
                                "JOIN studentresult sr ON sr.studentid = s.id " +
                                "JOIN assessmentcomponent asec ON sr.assessmentcomponentid = asec.id " +
                                "JOIN assessment ase ON asec.assessmentid = ase.id " +
                                "JOIN rubric r ON asec.rubricid = r.id " +
                                "JOIN clo c ON r.cloid = c.id " +
                                "JOIN rubriclevel rl ON rl.rubricid = r.id " +
                                "WHERE c.id = @id " +
                                "GROUP BY s.id, s.registrationNumber, s.firstname, s.lastname,c.name, ase.id, ase.title, ase.totalmarks, asec.name, asec.totalmarks, sr.rubricmeasurementid", con);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    cloWiseResult cloWiseResult = new cloWiseResult();
                    cloWiseResult.RegistrationNumber = reader["Registration No"].ToString();
                    cloWiseResult.StudentName = reader["Name"].ToString();
                    cloWiseResult.CLOName = reader["CLO"].ToString();
                    cloWiseResult.AssessmentTitle = reader["Assessment Title"].ToString();
                    cloWiseResult.AssessmentMarks = Convert.ToDouble(reader["Assessment Marks"]);
                    cloWiseResult.ComponentName = reader["Component Name"].ToString();
                    cloWiseResult.ComponentMarks = Convert.ToDouble(reader["Component Marks"]);
                    cloWiseResult.ObtainedMarks = Convert.ToDouble(reader["Obtained Marks"]);
                    result.Add(cloWiseResult);
                }
            }
            return result;
        }
        public class cloWiseResult
        {
            public string RegistrationNumber { get; set; }
            public string StudentName { get; set; }
            public string CLOName { get; set; }
            public string AssessmentTitle { get; set; }
            public string ComponentName { get; set; }
            public double AssessmentMarks { get; set; }
            public double ComponentMarks { get; set; }
            public double ObtainedMarks { get; set; }
        }

    }
}

