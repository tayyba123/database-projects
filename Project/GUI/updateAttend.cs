﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Project.GUI
{
    public partial class updateAttend : Form
    {
       bool isClick = false;
        string studid = "";
        string attendid = "";
        public updateAttend()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DateTime selectedDate = selectedDatepicker.Value;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.AttendanceID,s.StudentId,ss.RegistrationNumber as[ Registration No],concat(ss.firstname,' ',ss.lastname) as Name,l.name as Status from StudentAttendance s join ClassAttendance c on s.AttendanceID=c.id join Student ss on s.StudentID=ss.id join lookup l on s.AttendanceStatus=l.lookupid  where c.AttendanceDate=@date", con);
            cmd.Parameters.AddWithValue("@date", selectedDate);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            this.attendGrid.DataSource = dt;
            attendGrid.AllowUserToAddRows = false;
            isClick = true;
        }
        private void update()
        {

            try
            {
                foreach (DataGridViewRow row in this.attendGrid.Rows)
                {
                    attendid = row.Cells[1].Value.ToString();

                    if (row.Cells[0].Value == null)
                    {
                        studid= row.Cells[2].Value.ToString();
                        UpdateStudAttendence(0, studid);
                    }
                   else if (row.Cells[0].Value.ToString() == "PRESENT")
                    {
                        studid = row.Cells[2].Value.ToString();
                        UpdateStudAttendence(1, studid);
                    }
                    else if (row.Cells[0].Value.ToString() == "ABSENT")
                    {
                        studid = row.Cells[2].Value.ToString();
                        UpdateStudAttendence(2, studid);
                    }
                    else if (row.Cells[0].Value.ToString() == "LEAVE")
                    {
                        studid = row.Cells[2].Value.ToString();
                        UpdateStudAttendence(3, studid);
                    }
                    else if (row.Cells[0].Value.ToString() == "LATE")
                    {
                        studid = row.Cells[2].Value.ToString();
                        UpdateStudAttendence(4, studid);
                    }
                }
                MessageBox.Show("Updated Successfully");
                attendGrid.Refresh();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void UpdateStudAttendence(int status,string studid)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE StudentAttendance SET AttendanceStatus = CASE WHEN @stts = 0 THEN AttendanceStatus ELSE @stts END where StudentId=@studid and AttendanceId=@attendid", con);
            cmd.Parameters.AddWithValue("@stts", status);
            cmd.Parameters.AddWithValue("@studid", studid);
            cmd.Parameters.AddWithValue("@attendid", attendid);
            cmd.ExecuteNonQuery();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(isClick)
            {
                update();
            }
            else
            {
                MessageBox.Show("Nothing to Update...");
            }
        }

    }
}
