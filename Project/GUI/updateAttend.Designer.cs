﻿namespace Project.GUI
{
    partial class updateAttend
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.attendGrid = new System.Windows.Forms.DataGridView();
            this.att_status = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.selectedDatepicker = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.attendGrid)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.CadetBlue;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.attendGrid);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1292, 716);
            this.panel1.TabIndex = 0;
            // 
            // attendGrid
            // 
            this.attendGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.attendGrid.BackgroundColor = System.Drawing.Color.CadetBlue;
            this.attendGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.attendGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.att_status});
            this.attendGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.attendGrid.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.attendGrid.Location = new System.Drawing.Point(0, 270);
            this.attendGrid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.attendGrid.Name = "attendGrid";
            this.attendGrid.RowHeadersWidth = 62;
            this.attendGrid.Size = new System.Drawing.Size(1292, 446);
            this.attendGrid.TabIndex = 32;
            // 
            // att_status
            // 
            this.att_status.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.att_status.HeaderText = "Status";
            this.att_status.Items.AddRange(new object[] {
            "PRESENT",
            "ABSENT",
            "LEAVE",
            "LATE"});
            this.att_status.MinimumWidth = 8;
            this.att_status.Name = "att_status";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button1.BackColor = System.Drawing.Color.Teal;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Cinzel Black", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(515, 144);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(166, 49);
            this.button1.TabIndex = 31;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cinzel Black", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(312, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 27);
            this.label2.TabIndex = 29;
            this.label2.Text = "Select date";
            // 
            // button6
            // 
            this.button6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button6.BackColor = System.Drawing.Color.Teal;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Cinzel Black", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.Location = new System.Drawing.Point(743, 144);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(148, 49);
            this.button6.TabIndex = 28;
            this.button6.Text = "View";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // selectedDatepicker
            // 
            this.selectedDatepicker.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.selectedDatepicker.CalendarFont = new System.Drawing.Font("Cinzel Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedDatepicker.CalendarMonthBackground = System.Drawing.Color.CadetBlue;
            this.selectedDatepicker.CalendarTitleBackColor = System.Drawing.Color.Teal;
            this.selectedDatepicker.Font = new System.Drawing.Font("Cinzel Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedDatepicker.Location = new System.Drawing.Point(515, 75);
            this.selectedDatepicker.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.selectedDatepicker.Name = "selectedDatepicker";
            this.selectedDatepicker.Size = new System.Drawing.Size(376, 34);
            this.selectedDatepicker.TabIndex = 27;
            this.selectedDatepicker.Value = new System.DateTime(2023, 2, 21, 21, 46, 30, 0);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.selectedDatepicker);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.button6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1292, 270);
            this.panel2.TabIndex = 33;
            // 
            // updateAttend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1292, 716);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "updateAttend";
            this.Text = "updateAttend";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.attendGrid)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DateTimePicker selectedDatepicker;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView attendGrid;
        public System.Windows.Forms.DataGridViewComboBoxColumn att_status;
        private System.Windows.Forms.Panel panel2;
    }
}