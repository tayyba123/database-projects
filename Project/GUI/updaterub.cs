﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Project.GUI
{
    public partial class updaterub : Form
    {
        public updaterub()
        {
            InitializeComponent();
            addInComboBox();
            addInComboBoxofRubId();
            fillgrid();
        }
        private void addInComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select name from Clo", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    cmb_ruberics.Items.Add(rdr.GetString(0));
                }
            }
        }
        private void addInComboBoxofRubId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select details from Rubric", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr.GetString(0));

                }
            }
        }
        private int getrubIdFromName()
        {
            string assess = comboBox1.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from rubric where details=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }
        }
        private int getcloIdFromName()
        {
            string assess = cmb_ruberics.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from clo where name=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }
        }
        private void fillgrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select r.Id as [Rubric Id],r.details as [Rubric Details],r.cloid as [CLO Id],c.name as [CLO Name]from Rubric r join Clo c on c.id=r.cloid", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            int id = getrubIdFromName();
            int cloid = getcloIdFromName();
            if (id!=0)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE Rubric SET Details=CASE WHEN  @details ='' or @details is null THEN Details ELSE @details END,Cloid= CASE WHEN @cloid=0 THEN  Cloid ELSE  @cloid END where Id=@id", con);
                cmd.Parameters.AddWithValue("@details", txtname.Text);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@cloid", cloid);
                cmd.ExecuteNonQuery();
                dataGridView1.DataSource = null;
                fillgrid();
                MessageBox.Show("Successfully Updated");
                txtname.Text = "";
                comboBox1.Text = "";
                cmb_ruberics.Text = "";
                comboBox1.Items.Clear();
                cmb_ruberics.Items.Clear();
                addInComboBoxofRubId();
                addInComboBox();
            }
            else
            {
                label4.Text = "Please select rubric for Updation";
                panel7.Visible = true;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel7.Visible = false;
        }
    }
}
