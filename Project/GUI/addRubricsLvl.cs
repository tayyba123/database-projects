﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Project.GUI
{
    public partial class addRubricsLvl : Form
    {
        public addRubricsLvl()
        {
            InitializeComponent();
            addInComboBox();
            fillgrid();
        }
        private void addInComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select details from Rubric", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    cmb_ruberics.Items.Add(rdr.GetString(0));
                }
            }
        }
        private int getrubricIdFromName()
        {
            string assess = cmb_ruberics.Text;
            if (!string.IsNullOrWhiteSpace(assess))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select id from rubric where details=@assess", con);
                cmd.Parameters.AddWithValue("@assess", assess);
                return (int)cmd.ExecuteScalar();
            }
            else
            {
                return 0;
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            int rubId = getrubricIdFromName();
            string details=txtname.Text;
            string measLvl = numericUpDown1.Value.ToString();
            if (rubId!=0)
            {
                if (!string.IsNullOrWhiteSpace(details))
                {
                    if (!string.IsNullOrWhiteSpace(measLvl))
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("Insert into RubricLevel values (@rubid,@details, @measLvl)", con);
                        cmd.Parameters.AddWithValue("@rubid", rubId);
                        cmd.Parameters.AddWithValue("@details", details);
                        cmd.Parameters.AddWithValue("@measLvl", measLvl);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully Added");
                        txtname.Text = "";
                        cmb_ruberics.Text = "";
                        numericUpDown1.Value = 1;
                        cmb_ruberics.Items.Clear();
                        addInComboBox();
                        dataGridView1.DataSource = null;
                        fillgrid();
                    }
                    else
                    {
                        label5.Text = "Please enter Measurment level";
                        panel8.Visible = true;
                    }
                }
                else
                {
                    label3.Text = "Please enter details";
                    errorpnl.Visible = true;
                }
            }
            else
            {
                label7.Text = "Please select linked Rubric";
                panel10.Visible = true;
            }
        }
       
        private void fillgrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select l.Id as [Level Id],l.RubricId as [Rubric Id],r.Details as [Rubric Details],l.details as [Level Details],l.MeasurementLevel as Marks,c.id as [CLO Id] , c.name as [CLO Name] from RubricLevel l join rubric r on r.id=l.rubricid join clo c on r.cloid=c.id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
        }
          
        
    }
}
