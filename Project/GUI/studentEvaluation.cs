﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace Project.GUI
{
    public partial class studentEvaluation : Form
    {
        int assessid;
        int assesscompid;
        public studentEvaluation()
        {
            InitializeComponent();
            addInComboBox();
        }
        private Form activeForm = null;
        private void openChildFormSign(Form childFormSign)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = childFormSign;
            childFormSign.TopLevel = false;
            childFormSign.FormBorderStyle = FormBorderStyle.None;
            childFormSign.Dock = DockStyle.Fill;
            panel1.Controls.Add(childFormSign);
            panel1.Tag = childFormSign;
            childFormSign.BringToFront();
            childFormSign.Show();
        }

        private void addInComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select title from assessment", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr.GetString(0));

                }
            }

        }
        private int getassessIdFromName()
        {
            string assess = comboBox1.Text;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select id from assessment where title=@assess", con);
            cmd.Parameters.AddWithValue("@assess", assess);
            return (int)cmd.ExecuteScalar();

        }
        private int getassessCompIdFromName()
        {
            string assess = comboBox2.Text;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select id from assessmentcomponent where name=@assess", con);
            cmd.Parameters.AddWithValue("@assess", assess);
            return (int)cmd.ExecuteScalar();

        }
        private void addInComboBoxAssessComp()
        {
            assessid = getassessIdFromName();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select name from assessmentComponent where assessmentid=@id", con);
            cmd.Parameters.AddWithValue("@id", assessid);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox2.Items.Add(rdr.GetString(0));

                }
            }
        }
        private void loadStudents()
        {
            assesscompid = getassessCompIdFromName();
            this.dataGridView1.AllowUserToAddRows = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.id,s.registrationNumber as [Registration No],concat(s.firstname,' ',s.lastname) as Name from student s where s.id in(select s.id from student where s.status=5 except(select sr.studentid from studentresult sr join assessmentComponent ac on sr.assessmentcomponentid=ac.id where sr.assessmentcomponentid=@id))", con);
            cmd.Parameters.AddWithValue("@id", assesscompid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            this.dataGridView1.AllowUserToAddRows = false;

        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadStudents();
            MessageBox.Show("To Evaluate: Click the required cell of grid");
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            addInComboBoxAssessComp();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                    int studentId = int.Parse(row.Cells["id"].Value.ToString());
                    panel2.Visible = false;
                    panel3.Visible = false;
                    openChildFormSign(new evaluate(studentId, assesscompid));
                }
                else{
                    MessageBox.Show("No evaluations remaining for this assessment.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No Rubric/Rubric level added for this assessment.");
            }
        }
    }
}
