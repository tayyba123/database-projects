﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Project.GUI
{
    public partial class evaluation : Form
    {
        int marks;
        int sid;
        int assessid;
        int assesscompid;
        private string[] detailsArr = new string[10];   

        public evaluation(int sid,int assesid,int assesscompid)
        {
            InitializeComponent();
            this.sid = sid;
            this.assessid = assesid;
            this.assesscompid = assesscompid;
            loadStudents();
        }
        private int getid()
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select rubricid from assessmentcomponent where id=@id", con);
                cmd.Parameters.AddWithValue("@id", assesscompid);
                return (int)cmd.ExecuteScalar();
            }
            catch(Exception ex)
            {
                return 0;
            }
        }
        private void loadStudents()
        {
            try
            {
                this.dataGridView1.AllowUserToAddRows = true;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select id,name from assessmentcomponent where id=@id", con);
                cmd.Parameters.AddWithValue("@id", assesscompid);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

                int rubid = getid();
                if (rubid !=0)
                {
                    con = Configuration.getInstance().getConnection();
                    SqlCommand cmd2 = new SqlCommand("Select  STRING_AGG(convert(nvarchar(max),Details), ',') from rubriclevel where rubricid=@id", con);
                    cmd2.Parameters.AddWithValue("@id", rubid);
                    string details = (string)cmd2.ExecuteScalar();
                    detailsArr = details.Split(',');
                    foreach (string s in detailsArr)
                    {
                        DataGridViewButtonColumn detail = new DataGridViewButtonColumn();
                        detail.Name = s;
                        detail.HeaderText = s;

                        con = Configuration.getInstance().getConnection();
                        SqlCommand cmd3 = new SqlCommand("Select  measurementlevel from rubriclevel where rubricid=@id and details=@detail", con);
                        cmd3.Parameters.AddWithValue("@id", rubid);
                        cmd3.Parameters.AddWithValue("@detail", s);
                        object result = cmd3.ExecuteScalar();
                        int marks = result != null ? (int)result : 0;
                        detail.DefaultCellStyle.NullValue = marks.ToString();
                        detail.FlatStyle = FlatStyle.Flat;
                        dataGridView1.Columns.Add(detail);
                        dataGridView1.CellContentClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);
                    }
                this.dataGridView1.AllowUserToAddRows = false;
                    dataGridView1.ReadOnly = true;
                }
                else
                {
                    MessageBox.Show("No Rubric added for this asessment");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No Rubric level added for this asessment");
            }
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == dataGridView1.Columns[detailsArr[0]].Index)
                {
                    string buttonText = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
                    marks = int.Parse(buttonText);
                }
                else if (e.ColumnIndex == dataGridView1.Columns[detailsArr[1]].Index)
                {
                    string buttonText = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
                    marks = int.Parse(buttonText);
                }
                else if (e.ColumnIndex == dataGridView1.Columns[detailsArr[2]].Index)
                {
                    string buttonText = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
                    marks = int.Parse(buttonText);
                }
                if (e.ColumnIndex == dataGridView1.Columns[detailsArr[3]].Index)
                {
                    string buttonText = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
                    marks = int.Parse(buttonText);
                }
                else if (e.ColumnIndex == dataGridView1.Columns[detailsArr[4]].Index)
                {
                    string buttonText = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
                    marks = int.Parse(buttonText);
                }
            }
            catch(Exception ex) { }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into StudentResult values (@sid, @assesscompid,@measurmentid,@date)", con);
            cmd.Parameters.AddWithValue("@sid", sid);
            cmd.Parameters.AddWithValue("@assesscompid", assesscompid);
            cmd.Parameters.AddWithValue("@measurmentid",marks);
            cmd.Parameters.AddWithValue("@date",DateTime.Now);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }
    }
}
