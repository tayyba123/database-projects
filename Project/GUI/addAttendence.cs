﻿using project;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Project.GUI
{
    public partial class addAttendence : Form
    {
        int classID = 0;
        public addAttendence()
        {
            InitializeComponent();
            fillGrid();
        }
        private void fillGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select RegistrationNumber as [Registration No], concat(firstname,' ',lastname) as Name from Student where status= 5", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
           this. attendGrid.DataSource = dt;
            attendGrid.AllowUserToAddRows = false;
        }
       
        private void insertClassAttendence()
        {
            DateTime selectedDate = selectedDatepicker.Value;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into ClassAttendance  values (@AttendanceDate)", con);
            cmd.Parameters.AddWithValue("@AttendanceDate", selectedDate);
            cmd.ExecuteNonQuery();


            con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("select top(1) id from ClassAttendance  order by id desc", con);
            this.classID = (int)cmd2.ExecuteScalar();
        }
        private void insertStudAttendence(int regno,int stts)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into StudentAttendance  values (@id,@regno,@stts)", con);
            cmd.Parameters.AddWithValue("@regno", regno);
            cmd.Parameters.AddWithValue("@stts", stts);
            cmd.Parameters.AddWithValue("@id", classID);
            cmd.ExecuteNonQuery();
        }
        private  void insertStudAttendencerecord()
        {
            try
            {
                string regno = "";
                int studId = 0;
                foreach(DataGridViewRow row in this.attendGrid.Rows)
                {
                    regno = row.Cells[1].Value.ToString();
                    studId = GetId(regno);

                    if (row.Cells[0].Value == null) throw new Exception("Attendance not marked against some students");

                    else if (row.Cells[0].Value.ToString() == "PRESENT")
                    {
                        insertStudAttendence(studId, 1);
                    }
                    else if (row.Cells[0].Value.ToString() == "ABSENT")
                    {
                        insertStudAttendence(studId, 2);
                    }
                    else if (row.Cells[0].Value.ToString() == "LEAVE")
                    {
                        insertStudAttendence(studId, 3);
                    }
                    else if (row.Cells[0].Value.ToString() == "LATE")
                    {
                        insertStudAttendence(studId, 4);
                    }
                }
                MessageBox.Show("All Attendances have been marked for today");
                this.attendGrid.DataSource = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private int GetId(string regno)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select id from Student  where RegistrationNumber=@regno", con);
            cmd.Parameters.AddWithValue("@regno", regno);
            return (int)cmd.ExecuteScalar();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            insertClassAttendence();
            insertStudAttendencerecord();
        }
    }
}
