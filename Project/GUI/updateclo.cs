﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Project.GUI
{
    public partial class updateclo : Form
    {
        public updateclo()
        {
            InitializeComponent();
            fillgrid();
            addInComboBox();
        }
        private void addInComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select id from clo", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr.GetInt32(0).ToString());

                }
            }
        }
        private void fillgrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id,Name,Datecreated,Dateupdated from clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string id = comboBox1.Text;
            if (!string.IsNullOrWhiteSpace(id))
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE Clo SET Name=CASE WHEN  @name ='' or @name is null THEN Name ELSE @name END,DateCreated= CASE WHEN @creationDate='' or @creationDate is NULL THEN  DateCreated ELSE  @creationDate END where id=@id", con);
                cmd.Parameters.AddWithValue("@name", txtname.Text);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@creationDate", selectedDatepicker.Value);
                cmd.ExecuteNonQuery();
                dataGridView1.DataSource = null;
                fillgrid();
                MessageBox.Show("Successfully Updated");
            }
            else
            {
                label7.Text = "Please enter Id from Updation";
                panel10.Visible = true;
             }
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel10.Visible = false;
        }
    }
}
