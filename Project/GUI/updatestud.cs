﻿using project;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Collections.Specialized.BitVector32;
using System.Xml.Linq;

namespace Project.GUI
{
    public partial class updatestud : Form
    {
        public updatestud()
        {
            InitializeComponent();
            fillgrid();
            addInComboBox();
        }
        private void addInComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select id from student", con);
            using (SqlDataReader rdr = cmd.ExecuteReader())
            {
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr.GetInt32(0).ToString());
                }
            }
        }
        private void fillgrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.id,concat(s.firstname,' ',s.lastname) as Name,s.RegistrationNumber as [Registration No],s.contact,s.email,l.name as status from student s join lookup l on s.status=l.LookupId where l.lookupid= 5", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            string id = comboBox1.Text;
            string regno = txtname.Text;
            string fname = txt2ndname.Text;
            string secname = txtmail.Text;
            string mail = txtcontact.Text;
            string contact = txtreg.Text;
            if (!string.IsNullOrWhiteSpace(id))
            {
                using (var con = Configuration.getInstance().getConnection())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Student SET firstname = CASE WHEN @fname IS NULL OR @fname = '' THEN firstname ELSE @fname END, LastName = CASE WHEN @secname IS NULL OR @secname = '' THEN LastName ELSE @secname END, Contact = CASE WHEN @contact IS NULL OR @contact = '' THEN Contact ELSE @contact END, Email = CASE WHEN @mail IS NULL OR @mail = '' THEN Email ELSE @mail END , RegistrationNumber= case when  @regno is  NULL or @regno=' ' then RegistrationNumber else @regno end where id=@id", con);

                    cmd.Parameters.AddWithValue("@fname", fname);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@secname", secname);
                    cmd.Parameters.AddWithValue("@mail", mail);
                    cmd.Parameters.AddWithValue("@contact", contact);
                    cmd.Parameters.AddWithValue("@regno", regno);
                    cmd.ExecuteNonQuery();
                    dataGridView1.DataSource = null;
                    fillgrid();
                    MessageBox.Show("Successfully updated");

                }
            }
            else
            {
                label12.Text = "Please enter ID for updation";
                panel17.Visible = false;
            }
        }

        private void txtname_TextChanged_1(object sender, EventArgs e)
        {
            errorpnl.Visible = false;
        }

        private void txt2ndname_TextChanged_1(object sender, EventArgs e)
        {
            panel5.Visible = false;
        }

        private void txtmail_TextChanged_1(object sender, EventArgs e)
        {
            panel11.Visible = false;
        }

        private void txtcontact_TextChanged_1(object sender, EventArgs e)
        {
            panel8.Visible = false;
        }

        private void txtreg_TextChanged_1(object sender, EventArgs e)
        {
            panel14.Visible = false;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel17.Visible = false;
        }
    }
}
